const easymidi = require('easymidi');

const OUTPUT_NAME = 'loopMIDI Port 2';

// const output = new easymidi.Output(easymidi.getOutputs()[2]);
const output = new easymidi.Output(OUTPUT_NAME);
output.send('cc', {
    controller: 37,
    value: 0,
    channel: 0
  })