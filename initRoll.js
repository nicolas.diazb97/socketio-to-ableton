const easymidi = require('easymidi');

const OUTPUT_NAME = 'Microsoft GS Wavetable Synth 0';

const output = new easymidi.Output(easymidi.getOutputs()[2]);
output.send('cc', {
    controller: 36,
    value: 0,
    channel: 0
  })