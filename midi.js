const easymidi = require('easymidi');

const OUTPUT_NAME = 'Microsoft GS Wavetable Synth 0';

const output = new easymidi.Output(easymidi.getOutputs()[2]);
const pitchid = 37;
const yawid = 36;
const rollid = 35;
output.send('cc', {
    controller: 37,
    value: 127,
    channel: 0
  })
  output.send('cc', {
      controller: 36,
      value: 0,
      channel: 0
    })
    output.send('cc', {
        controller: 35,
        value: 0,
        channel: 0
      })